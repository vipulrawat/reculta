const publicRoutes = require('express').Router();
const User = require('../db/schemas/user');

publicRoutes.get('/', async (req, res) => {
  try {
    const user = await User.findById(req.session.userId);
    return res.json({ username: user.username, role: user.role });
  } catch (e) {
    return res.status(401).send('Please login first'); // not logged in
  }
});

module.exports = publicRoutes;
