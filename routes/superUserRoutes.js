const superUserRoutes = require('express').Router();
const User = require('../db/schemas/user');
const { can } = require('../utils/rbac');

superUserRoutes.post('/', async (req, res) => {
  const { username, role } = req.body;
  try {
    if (!req.session.userRole) {
      return res.sendStatus(401);
    }
    if (!can(req.session.userRole, 'write')) {
      return res.sendStatus(401);
    }
    const userToUpdate = await User.findOneAndUpdate({ username }, { $set: { role } });
    if (!userToUpdate) {
      return res.status(500).send('Unable to update');
    }
    return res.send(`${username} is assigned as ${role}`);
  } catch (e) {
    return res.sendStatus(500);
  }
});

module.exports = superUserRoutes;
