const managerRoutes = require('express').Router();
const User = require('../db/schemas/user');
const { can } = require('../utils/rbac');

managerRoutes.get('/', async (req, res) => {
  try {
    if (!req.session.userRole) {
      return res.sendStatus(401);
    }
    if (!can(req.session.userRole, 'read')) {
      return res.sendStatus(401);
    }
    const allUsers = await User.find({});
    return res.json({ allUsers });
  } catch (e) {
    return res.sendStatus(500);
  }
});

module.exports = managerRoutes;
