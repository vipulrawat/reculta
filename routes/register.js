const routes = require('express').Router();
const bcrypt = require('bcrypt');
const User = require('../db/schemas/user');
const limitUser = require('../utils/limitUsers');

function generateRandomString() {
  return Math.random().toString(36).slice(-8);
}

routes.post('/register', limitUser, async (req, res) => {
  if (req.userNum > 4) {
    return res.status(400).send('User limit exceeded');
  }
  const { username } = req.body;
  const password = generateRandomString();
  // console.log({ username, password });
  try {
    await User.create({ username, password });
    return res.json({ msg: 'Registered' });
  } catch (e) {
    return res.status(500).send('Error in regitering the user');
  }
});

routes.post('/login', async (req, res) => {
  const { username, password } = req.body;
  if (!username || !password) return res.status(400).send({ msg: 'Username and Password required' });
  try {
    const user = await User.findOne({ username });
    const match = await bcrypt.compare(password, user.password);
    if (!match) {
      return res.sendStatus(401);
    }
    req.session.userRole = user.role;
    req.session.userId = user._id;  // eslint-disable-line
    return res.redirect('/profile');
  } catch (e) {
    return res.sendStatus(401);
  }
});

routes.get('/logout', (req, res, next) => {
  if (req.session) {
    req.session.destroy((err) => {
      if (!err) {
        return res.send('Logged out successfully');
      }
      return next(err);
    });
  }
});


module.exports = routes;
