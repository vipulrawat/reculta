const User = require('../db/schemas/user');

async function limitUsers(req, res, next) {
  try {
    const userNumbers = await User.count({});
    req.userNum = userNumbers;
    next();
  } catch (e) {
    next(e);
  }
}

module.exports = limitUsers;
