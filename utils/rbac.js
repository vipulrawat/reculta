const roles = {
  superuser: {
    can: ['read', 'write', 'delete'],
  },
  admin: {
    can: ['read', 'delete'],
  },
  manager: {
    can: ['read'],
  },
  guest: {
    can: [],
  },
};

function can(role, operation) {
  return roles[role] && roles[role].can.indexOf(operation) !== -1;
}

module.exports = { roles, can };
