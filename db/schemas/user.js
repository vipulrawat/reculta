
const { Schema, model } = require('mongoose');
const bcrypt = require('bcrypt');

const User = new Schema({
  username: {
    type: String,
    required: true,
    unique: true,
    trim: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    default: 'guest',
    unique: true,
    enum: ['admin', 'manager', 'guest', 'superuser'],
  },
  isSuperUser: {
    type: Boolean,
  },
});
// eslint-disable-next-line
User.pre('save', function (next) {
  const user = this;
  // eslint-disable-next-line
  bcrypt.genSalt(10, function (err, salt) {
    if (err) return next(err);
    // eslint-disable-next-line
    bcrypt.hash(user.password, salt, function (err, hash) {
      if (err) return next(err);
      user.password = hash;
      next();
    });
  });
});

module.exports = model('User', User);
