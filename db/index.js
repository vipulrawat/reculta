const mongoose = require('mongoose');

const mongoURI = 'mongodb://localhost:27017/';

mongoose.connect(mongoURI, (err) => {
  if (err) {
    console.log('Error connecting with Database'); // eslint-disable-line
  } else {
    console.log('Database connected'); // eslint-disable-line
  }
});
