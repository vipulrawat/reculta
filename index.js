require('./db');
const app = require('express')();
const bodyParser = require('body-parser');
const session = require('express-session');
const registerRoutes = require('./routes/register');
const globalErrorHandler = require('./utils/errorHandler');
const adminRoutes = require('./routes/adminRoutes');
const publicRoutes = require('./routes/publicRoutes');
const superUserRoutes = require('./routes/superUserRoutes');
const managerRoutes = require('./routes/managerRoutes');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(session({
  secret: 'work hard',
  resave: true,
  saveUninitialized: false,
}));

app.use('/', registerRoutes);
app.use('/admin', adminRoutes);
app.use('/profile', publicRoutes);
app.use('/su', superUserRoutes);
app.use('/manager', managerRoutes);

app.use(globalErrorHandler);

const PORT = process.env.NODE_ENV === 'production' ? 80 : 4000;
app.listen(PORT, () => console.log(`Server is listening on: ${PORT}`)); // eslint-disable-line
