There are 4 users in a system with different roles. One user can only be assigned one role. The login IDs and passwords of the users who have been assigned these roles have been stored locally. We need to authenticate when any user tries to login and accordingly grant/deny access.

The task would be to build the following APIs (in Node.js) which are required to achieve this objective: 
1. Signup - Passwords should be randomly generated at the time of signup. 
2. Login 
3. One sample API for each user role

Please note that the system should be horizontally scalable.