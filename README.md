## Running

After cloning (and running you local mongo instance):
 - `yarn` or `npm install`
 - `yarn start` or `npm run start`

## Roles

I have given 4 roles as SuperUser, Admin, Manager and Guest. The permission provided are as follows:

 - *Super User*: The person that can assign roles to other user (one role per person), read all the users, and delete any one
 - *Admin*: Admin can read all the user and delete any one but cannot give roles to other
 - *Manager*: Manager can only read all the registered user.
 - *Guest*: Guest can only see his/her profile information

## Routes

 - *Super User*: POST `localhost:4000/su`, GET `localhost:4000/admin`, DELETE `localhost:4000/admin`  
 - *Admin*: GET `localhost:4000/admin`, DELETE `localhost:4000/admin`
 - *Manager*: GET `localhost:4000/manager`
 - *Guest*: GET `localhost:4000/profile`

## IMP: The problem statement mentioned that password should be randomly generated. So, You can know password of the user until you either send him through some medium (email) or for this project `console.log` before hashing the password. So in `routes/register.js` one can uncomment line#16 to note the password for login purpose.
